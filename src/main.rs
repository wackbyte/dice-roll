use {
    rand::Rng,
    std::{
        io::{self, stdin},
        thread::sleep,
        time::Duration,
    },
};

fn input() -> io::Result<String> {
    let mut buffer = String::new();
    stdin().read_line(&mut buffer)?;
    Ok(buffer.trim().to_string())
}

fn entry() -> io::Result<u32> {
    loop {
        match input()?.parse::<u32>() {
            Ok(guess) => break Ok(guess),
            Err(_) => {
                println!("That's not a number.");
            }
        }
    }
}

fn main() -> io::Result<()> {
    let mut rng = rand::thread_rng();

    'outer: loop {
        println!("What's the maximum number?");
        let max = loop {
            match entry()? {
                0 => {
                    println!("It must be greater than zero.");
                }
                entry => break entry,
            }
        };
        println!("How many rolls?");
        let rolls = entry()?;

        println!("Rolling...");
        for _ in 0..rolls {
            println!("{}", rng.gen_range(1..=max));
            sleep(Duration::from_millis(100));
        }

        'inner: loop {
            println!("Want to go again? (y/n)");
            match input()?.to_lowercase().as_str() {
                "y" | "ye" | "yes" => {
                    break 'inner;
                }
                "n" | "no" => {
                    break 'outer;
                }
                _ => {}
            }
        }
    }

    Ok(())
}
